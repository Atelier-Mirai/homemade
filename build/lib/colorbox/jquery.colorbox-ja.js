/*
  jQuery Colorbox language configuration
  language: Japanaese (ja)
  translated by: Hajime Fujimoto
*/
jQuery.extend(jQuery.colorbox.settings, {
	current: "({current}/{total})",
	previous: "‹",
	next: "›",
	close: "×",
	xhrError: "画像の読込に失敗しました",
	imgError: "画像の読込に失敗しました",
	slideshowStart: "▶",
	slideshowStop: "■"
});
