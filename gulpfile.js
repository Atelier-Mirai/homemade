// require-dir モジュールの読み込み
const requireDir    = require('require-dir');

// 各パッケージの読み込み
// 参考 https://hyper-text.org/archives/2018/03/gulp_quick_start.shtml

// Gulp 本体
const gulp          = require('gulp');
// ベンダプリフィクス管理
const autoprefixer  = require('gulp-autoprefixer');
// ブラウザ同期
const browserSync   = require('browser-sync');
// PostCSS のプラグイン。CSS ファイルをインポートする機能を提供
const cssimport     = require('postcss-import');
// CSS を圧縮 （Minify）
const cssmin        = require('gulp-cssmin');
// CSS verify
const cssv          = require('gulp-csslint');
// PostCSS のプラグイン。変数を使いたい
const cssvariables  = require('postcss-css-variables');
// Color 関数が使える
const colorfunction = require('postcss-color-function');
// Coffee Script
const coffee        = require('gulp-coffee');
// js の連結用
const concat        = require('gulp-concat');
// html verify
const htmlv         = require('gulp-htmlhint');
// 画像の最適化
const imagemin      = require('gulp-imagemin');
// 分散したメディアクエリの統合
const mqpacker      = require('css-mqpacker');
// デスクトップへ通知
const notify        = require('gulp-notify');
// watch タスク実行中エラーでも watch タスクを継続
const plumber       = require('gulp-plumber');
// PostCSS
const postcss       = require('gulp-postcss');
// ファイル名変更
const rename        = require('gulp-rename');
// Slim
const slim          = require('gulp-slim');
// JavaScript圧縮(Minify)
const uglify        = require('gulp-uglify');

// 各タスクの読み込み
requireDir('./gulp/tasks');

// default
gulp.task('default', gulp.series('watch'));
// gulp.task('default', gulp.series('watch', 'browser-sync'));
