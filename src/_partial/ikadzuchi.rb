module Ikadzuchi
  class << self
    # 工事名ディレクトリにある画像(.jpg)と、memo.csv、
    # template.slimをを雛形にslimファイルを生成する
    # @param [String] construction_name 工事名称
    # @return "#{construction_name}.slim" ファイルが生成される
    def generate(construction_name, pagination: true, base_image_dir: '../assets/images/')
      # 雛形ファイルを読み込む
      template = Ikadzuchi::template("#{base_image_dir}#{construction_name}/_template.slim", pagination)
      # 工事名称を読み込む
      info = Ikadzuchi::info("#{base_image_dir}#{construction_name}/_information.txt")
      # 備忘録を読み込む
      memo = Ikadzuchi::memo("#{base_image_dir}#{construction_name}/_memo.csv")
      # 画像ファイル名を取得
      images = Ikadzuchi::images("#{base_image_dir}#{construction_name}")
      # memoとimagesを併合
      images_with_memo = images.map.with_index do |image, i|
        { image: image }.merge memo[i]
      end
      # [{:image=>"../assets/images/ot/ot01.jpg", :caption=>"高圧洗浄", :comment=>""},
      #  {:image=>"../assets/images/ot/ot02.jpg", :caption=>"外壁の高圧洗浄", :comment=>""},
      #  {:image=>"../assets/images/ot/ot03.jpg", :caption=>"屋根の現状1", :comment=>""}]

      # 出力されるslim
      if pagination
        slim = Ikadzuchi::replace_with_pagination(template, info, images_with_memo, 12)
      else
        slim = Ikadzuchi::replace_without_pagination(template, info, images_with_memo)
      end

      # 結果を出力
      Ikadzuchi::write "_#{construction_name}.slim", slim
    end

    # ページネーションを出力する
    # « ‹ 8 9 10 11 12 13 14 › »
    # @params [Integer] current_page: 現在の頁数
    # @params [Integer] total_pages:  総頁数
    # @params [Integer] window:       前後にいくつの頁を表示させるか？
    def pagination(current_page:, total_pages:, window: 3)
      # 一頁しかないなら、ページネーション不要
      return if current_page == 1 && total_pages == 1

      pagination = ""
      if current_page != 1
        pagination << <<~EOH
          li.page-item
            a href="\#page1" &laquo;
          li.page-item
            a href="\#page#{current_page.pred}" &lsaquo;
        EOH
      end

      (([current_page-window, 1].max) .. [current_page+window, total_pages].min).each do |i|
        pagination << <<~EOH
          li.page-item#{i == current_page ? '.active' : ''}
            a href="\#page#{i}" #{i}
        EOH
      end

      if current_page != total_pages
        pagination << <<~"EOH"
          li.page-item
            a href="\#page#{current_page.next}" &rsaquo;
          li.page-item
            a href="\#page#{total_pages}" &raquo;
        EOH
      end

      "\n  .pagination\n" << pagination.split("\n").map { |line| "    #{line}" }.join("\n") << "\n\n"
    end

    # 指定されたディレクトリ内の画像ファイル名を返す
    # @param [String] directory 画像ファイルの置かれているディレクトリ名
    # @return [Array] 画像ファイル名
    def images(directory)
      # 画像ファイル名を取得
      # Dir.glob("#{directory}/*.jpg").map { |f| File.split(f)[1] }.sort
      Dir.glob("#{directory}/*.jpg").sort
    end

    # 指定された雛形を読み込む
    # @param [String] filename 雛形ファイル名
    # @return [Template] シングルトン
    def template(filename, pagination)
      Template.read(filename, pagination)
    end

    # 指定された備忘録を読み込む
    # @param [String] filanem 備忘録のファイル名
    # @return [Array] 読み込んだ備忘録
    def memo(filename)
      # カラム名の読み込み
      column_names = []
      memo = File.read filename
      # return memo
      memo.each_line do |line|
        # 先頭行はカラム名なので読み取る
        column_names = line.chop.tr('"', '').tr("'", '').split(',')
        break
      end

      array = []
      # 各フィールドを読み込み、arrayに格納する
      memo.each_line.with_index do |line, i|
        next if i == 0 # 先頭行はカラム名なので読み飛ばす

        columns = line.chop.tr('"', '').tr("'", '').split(',')
        hash = {}
        column_names.each_with_index do |c, i|
          hash.store column_names[i].to_sym, columns[i].to_s
        end
        array.push hash
      end
      array
    end

    # 指定された工事情報を読み込む
    # @param [String] filanem 工事情報のファイル名
    # @return [Hash] 読み込んだ工事情報
    def info(filename)
      info = File.read filename

      hash = {}
      info.each_line do |line|
        key, value = line.chop.tr('：', ':').split(':')
        case key
        when '工事名称'
          hash.store(:construction_name, value)
        end
      end
      hash
    end

    # 雛形を置換する
    # @param [String] template 雛形
    # @param [String] hash 置換用 key/value
    # @return [String] 置換後のtemplate
    def replace(template, hash)
      t = template
      hash.each do |key, value|
        t = t.gsub("\#{#{key}}", value )
      end
      t
    end

    # 雛形を置換する
    # @param [String] template 雛形
    # @param [String] array 置換用 key/value hashの配列
    # @param [Integer] per_page  一頁に何枚の写真を表示させるか
    # @return [String] 置換後のtemplate
    def replace_with_pagination(template, info, images_with_memo, per_page = 12)
      current_page = 1
      total_pages = (images_with_memo.count / per_page.to_f).ceil
      # 施工写真達の先頭に工事名称生成
      slim = Ikadzuchi::replace(template.header, info)
      slim << "\n"

      images_with_memo.each.with_index(1) do |e, i|
        if i % per_page == 1
          slim << "\#page#{current_page}.page-content#{ current_page == 1 ? '.active' : '' }\n"
        end

        # 施工写真達の先頭にページネーション生成
        if i % per_page == 1
          slim << pagination(current_page: current_page, total_pages: total_pages)
        end

        slim << Ikadzuchi::replace(template.body, e)

        # 施工写真達の末尾にページネーション生成
        if i % per_page == 0
          slim << pagination(current_page: current_page, total_pages: total_pages)
          current_page += 1
        end
      end
      # 最終頁の末尾にページネーション生成
      if per_page * total_pages != images_with_memo.count
        slim << pagination(current_page: current_page, total_pages: total_pages)
      end
      slim
    end

    # 雛形を置換する
    # @param [String] template 雛形
    # @param [String] array 置換用 key/value hashの配列
    # @return [String] 置換後のtemplate
    def replace_without_pagination(template, array)
      slim = ""
      array.each do |a|
        slim << Ikadzuchi::replace(template, a)
      end
      slim
    end

    # ファイルを出力する
    # @params [String] filename
    # @params [String] text
    def write(filename, text)
      File.write filename, text
    end
  end
end

require 'singleton'
class Template
  include Singleton
  attr_accessor :header, :body

  def initialize
    @template = ""
  end

  class << self
    def read(filename, pagination)
      template = Template.instance
      template.header = ""
      template.body = ""

      # テンプレートファイルを読み込む
      t = File.read filename

      mode = :header
      t.each_line do |line|
        if line =~ /^\R/
          mode = :body
          next
        end
        case mode
        when :header
          template.header << line
        when :body
          template.body << line
        end
      end

      if pagination
        # 行頭に二つ半角空白を附与
        str = ""
        template.body.each_line do |line|
          str << "  #{line}"
        end
        template.body = str
      end
      template
    end
  end
end
