// (($ => {
//   $.fn.datepicker = function (options) {
//     // 設定事項
//     const settings = $.extend({
//       dateFormat: "YYYY-MM-DD",
//       locale: 'ja',
//       positionShift: { top: 31, left: 0 },
//     }, options);
//     moment.locale(settings.locale);
//
//     // 関数本体
//     return this.each((i, elem) => {
//       elem = $(elem);
//
//       let selectDate = "";
//       let field_value = elem.val();
//       if (field_value == ""){
//         selectDate = moment();
//       } else {
//         selectDate = moment(field_value);
//       }
//       let lastSelected = copyDate(selectDate);
//
//       // 手動でinputに入力したときに日付が反映される為に
//       elem.on('change', () => {
//         field_value  = elem.val();
//         selectDate   = moment(field_value);
//         lastSelected = copyDate(selectDate);
//       });
//
//       elem.on('click', () => {
//         const $body    = $('body');
//         const $win     = $('<div>', { class: 'dp_modal-win' });
//         const $content = createContent();
//         const offset   = elem.offset();
//         $body.append($win).append($content);
//
//         // 右端にdatepickerがあったときに、
//         // 右端が欠けないように適宜左に寄った位置から表示する
//         const width_total = offset.left + settings.positionShift.left + $content.width();
//         const difference  = $win.width() - width_total
//         if (difference >= 0){
//           $content.css({
//             top:  `${offset.top  + settings.positionShift.top}px`,
//             left: `${offset.left + settings.positionShift.left}px`
//           });
//         } else {
//           $content.css({
//             top:  `${offset.top  + settings.positionShift.top}px`,
//             left: `${offset.left + settings.positionShift.left + difference - 20}px`
//           });
//         }
//
//         feelDates(selectDate);
//         $win.on('click', () => {
//           $content.remove();
//           $win.remove();
//         });
//
//         function feelDates(selectDate) {
//           $content.empty()
//                   .append(createMonthPanel(selectDate))
//                   .append(createCalendar(selectDate));
//         }
//
//         function createMonthPanel(selectMonth) {
//           const $d = $('<div>', { class: 'calendar-nav navbar' });
//           $('<button>', {
//             class: 'btn btn-action btn-link btn-lg',
//             html:  '<i class="icon icon-arrow-left">',
//             on:    { click: prevMonth }
//           }).appendTo($d);
//
//           let year  = selectMonth.year() - 1988;
//           let month = selectMonth.month() + 1;
//           $('<div>', {
//             class: 'navbar-primary',
//             text:  `平成${year}年 ${month}月`
//           }).appendTo($d);
//
//           $('<button>', {
//             class: 'btn btn-action btn-link btn-lg',
//             html:  '<i class="icon icon-arrow-right">',
//             on:    { click: nextMonth }
//           }).appendTo($d);
//           return $d;
//         }
//
//         function nextMonth() {
//           selectDate.add(1, 'month');
//           feelDates(selectDate);
//         }
//
//         function prevMonth() {
//           selectDate.subtract(1, 'month');
//           feelDates(selectDate);
//         }
//
//         function createCalendar(selectDate) {
//           // 曜日毎のclassをセットする為に
//           const w = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']
//           let m  = copyDate(selectDate);
//           // 月初からcalendar表示できるように
//           m.date(1);
//           // その月の日数
//           const daysInMonth = m.daysInMonth();
//           // 月末以降の空欄の数
//           const daysInNextMonth = 7 - (m.weekday()+daysInMonth)%7;
//
//           // calendar-container
//           let $c = $('<div>', { class: 'calendar-container' });
//
//           // calendar-header
//           let $h = $('<div>', { class: 'calendar-header' });
//           ['日', '月', '火', '水', '木', '金', '土'].forEach((d, i) => {
//             $('<div>', {
//               class: 'calendar-date',
//               addClass: w[i],
//               text: d
//             }).appendTo($h);
//           });
//           $h.appendTo($c);
//
//           // calendar-body
//           let $b = $('<div>', { class: 'calendar-body' });
//           // 1日までの空欄を作成
//           for (let i = 0; i < m.weekday(); i++) {
//             $('<div>', { class: 'calendar-date' }).appendTo($b);
//           }
//           // 当月分の日付を作成
//           for (let i = 1; i <= daysInMonth; i++){
//             let d = w[m.weekday()];
//             let h = isHoliday(m) ? 'holiday' : '';
//             let t = m.isSame(moment(), 'day') ? 'date-today' : '';
//             let s = m.isSame(lastSelected, 'day') ? 'select-date' : '';
//             $('<div>', {
//               class: `calendar-date`,
//               html:  $('<button>', {
//                         class: `date-item ${d} ${h} ${t} ${s}`,
//                         text: `${i}`
//                      }).on('click', changeDate)
//             }).appendTo($b);
//             m.add(1, 'days');
//           }
//           // 翌月までの空欄を作成
//           for (let i = 0; i < daysInNextMonth; i++) {
//             $('<div>', { class: 'calendar-date' }).appendTo($b);
//           }
//           $b.appendTo($c);
//           return $c;
//         }
//
//         function changeDate() {
//           const $btn = $(this);
//           selectDate.date($btn.text()); // calendarのクリックした日がセットされる
//           lastSelected = copyDate(selectDate);
//           updateDate();
//           const old = $content.find('.select-date');
//           old
//             .removeClass('select-date')
//             .on('click', changeDate);
//           $btn
//             .addClass('select-date')
//             .off('click');
//         }
//
//         function createContent() {
//           return $('<div>', { class: 'dp_modal-content calendar' });
//         }
//
//         function updateDate() {
//           elem.val(lastSelected.format(settings.dateFormat));
//         }
//       });
//     });
//   };
//
//   function copyDate(d) {
//     return moment(d.toDate());
//   }
//
//   function isHoliday(m) {
//     const holidays = [
//       moment("2019-01-01"),
//       moment("2019-01-14"),
//       moment("2019-02-11"),
//       moment("2019-03-21"),
//       moment("2019-04-29"),
//       moment("2019-04-30"),
//       moment("2019-05-01"),
//       moment("2019-05-02"),
//       moment("2019-05-03"),
//       moment("2019-05-04"),
//       moment("2019-05-05"),
//       moment("2019-05-06"),
//       moment("2019-07-15"),
//       moment("2019-08-11"),
//       moment("2019-08-12"),
//       moment("2019-09-16"),
//       moment("2019-09-23"),
//       moment("2019-10-14"),
//       moment("2019-10-22"),
//       moment("2019-11-03"),
//       moment("2019-11-04"),
//       moment("2019-11-23")
//     ];
//     for(let d of holidays){
//       if(m.isSame(d, 'day')){
//         return true;
//       }
//     }
//     return false
//   }
// })(jQuery));
