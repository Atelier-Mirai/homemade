const images      = 'assets/images/';
const javascripts = 'assets/javascripts/';
const stylesheets = 'assets/stylesheets/';
const scss        = 'assets/scss/';

// モジュールの定義
module.exports = {
  // 出力先の指定
  build:             './build/',
  build_images:      './build/' + images,
  build_javascripts: './build/' + javascripts,
  build_stylesheets: './build/' + stylesheets,
  // 入力元の指定
  src:               './src/',
  src_images:        './src/' + images,
  src_javascripts:   './src/' + javascripts,
  src_stylesheets:   './src/' + stylesheets,
  scss:              './src/' + scss,
  // partialファイルを格納するディレクトリ
  partial:           './src/_partial/'
}
