const gulp        = require('gulp');
const browserSync = require('browser-sync');
const cssmin      = require('gulp-cssmin');
const notify      = require('gulp-notify');
const plumber     = require('gulp-plumber');
const rename      = require('gulp-rename');

const config      = require('../config.js'); // config.js の読み込み

// cssmin
gulp.task('cssmin', (done) => {
  gulp.src(config.build_stylesheets + '*.css')
  .pipe(plumber({
    errorHandler: notify.onError('Error: <%= error.message %>')
  }))
  .pipe(cssmin())
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(config.build_stylesheets))
  .pipe(browserSync.stream());
  done();
});
