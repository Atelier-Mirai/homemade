const gulp         = require('gulp');
const autoprefixer = require('autoprefixer');
const browserSync  = require('browser-sync');
const cssmin       = require('gulp-cssmin');
const notify       = require('gulp-notify');
const plumber      = require('gulp-plumber');
const postcss      = require('gulp-postcss');
const rename       = require('gulp-rename');
const sass         = require('gulp-sass');
const sassGlob     = require('gulp-sass-glob');

const config       = require('../config.js'); // config.js の読み込み

gulp.task('sass', (done) => {
  gulp.src([config.src + '/assets/stylesheets/*.scss'])
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    // scss -> css
    .pipe(sassGlob())
    .pipe(sass({ outputStyle: 'expanded' }))
    .pipe(postcss([
      autoprefixer({
        grid: true,
        browers: [
        '> 1% in JP',
        // 'last 8 versions',
        // 'last 2 versions',
        // 'iOS >= 10',
        // 'Android >= 6'
        ]
      })
     ]))
    // css -> minify
    // .pipe(cssmin())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(config.build + '/assets/stylesheets'))
    .pipe(browserSync.stream());
    done();
});
