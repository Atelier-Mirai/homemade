const gulp    = require('gulp');
const babili  = require('gulp-babili');
const concat  = require('gulp-concat');
const notify  = require('gulp-notify');
const plumber = require('gulp-plumber');
const rename  = require('gulp-rename');

const config  = require('../config.js'); // config.js の読み込み

// minify
gulp.task('minify', (done) => {
  gulp.src(config.src_javascripts + '/*.js')
  .pipe(plumber({
    errorHandler: notify.onError('Error: <%= error.message %>')
  }))
  .pipe(concat('all.js'))
  .pipe(babili({
    mangle: {
      keepClassName: true
    }
  }))
  .pipe(rename({suffix: '.min'}))
  .pipe(gulp.dest(config.build_javascripts));
  done()
});
