const gulp   = require('gulp');
const pump   = require('pump');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');

const config = require('../config.js'); // config.js の読み込み

// uglify
gulp.task('uglify', (done) => {
  pump(
    [
      gulp.src(config.build_javascripts + '*.js'),
      uglify(),
      rename({suffix: '.min'}),
      gulp.dest(config.build_javascripts)
    ],
    done()
  );
});
