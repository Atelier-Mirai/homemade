const gulp    = require('gulp');
const watch   = require('gulp-watch');

const config  = require('../config.js');

gulp.task('watch', done => {
  gulp.watch([config.src_stylesheets + '*.scss', config.scss + '**/*.scss'],
    gulp.series("sass")
  );
  gulp.watch([config.src + '*.slim', config.partial + '*.slim'],
    gulp.series("slim")
  );
  gulp.watch([config.src_javascripts + '*.js'],
    gulp.series("minify")
  );
  done();
});
