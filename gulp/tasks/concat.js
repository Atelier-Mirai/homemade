const gulp    = require('gulp');
const concat  = require('gulp-concat');
const notify  = require('gulp-notify');
const plumber = require('gulp-plumber');

const config  = require('../config.js'); // config.js の読み込み

// concat
gulp.task('concat', (done) => {
  gulp.src(config.src_javascripts + '/*.js')
  .pipe(plumber({
    errorHandler: notify.onError('Error: <%= error.message %>')
  }))
  .pipe(concat('all.js'))
  .pipe(gulp.dest(config.build_javascripts));
  done();
});
