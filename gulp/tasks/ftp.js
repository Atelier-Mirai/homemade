// https://www.webprofessional.jp/pass-parameters-gulp-tasks/

// usage:
// npx gulp ftp -user myuserid -password mypassword
// npx gulp ftp -u myuserid -p mypassword

// fetch command line arguments
const arg = (argList => {
  let arg = {}, a, opt, thisOpt, curOpt;
  for (a = 0; a < argList.length; a++) {
    thisOpt = argList[a].trim();
    opt = thisOpt.replace(/^\-+/, '');
    if (opt === thisOpt) {
      // argument value
      if (curOpt) arg[curOpt] = opt;
      curOpt = null;
    } else {
      // argument name
      curOpt = opt;
      arg[curOpt] = true;
    }
  }
  return arg;
})(process.argv);

// ftp
const gulp = require('gulp');
const ftp  = require('vinyl-ftp');

gulp.task('ftp', () => {
  let connection = ftp.create({
      host    : 'mysite.com',           // サイト名
      user    : arg.user || arg.u,      // command line option
      password: arg.password || arg.p,  // command line option
      parallel: 5
    });
  let glob = ['build/**/*'];
  let src = {
        base  : 'build/',
        buffer: false
      };
  let remotePath = '/public_html/';

  return gulp.src(glob, src)
    .pipe(connection.newerOrDifferentSize(remotePath))
    .pipe(connection.dest(remotePath));
});
