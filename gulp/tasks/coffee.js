const gulp          = require('gulp');
const browserSync   = require('browser-sync');
const coffee        = require('gulp-coffee');
const notify        = require('gulp-notify');
const plumber       = require('gulp-plumber');

const config       = require('../config.js'); // config.js の読み込み

// coffee
gulp.task('coffee', (done) => {
  gulp.src(config.src_javascripts + '*.coffee')
  .pipe(plumber({
    errorHandler: notify.onError('Error: <%= error.message %>')
  }))
  .pipe(coffee({bare: true}))
  .pipe(gulp.dest(config.src_javascripts))
  .pipe(browserSync.stream());
  done();
});
