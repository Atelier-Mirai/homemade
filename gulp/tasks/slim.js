const gulp        = require('gulp');
const browserSync = require('browser-sync');
const notify      = require('gulp-notify');
const plumber     = require('gulp-plumber');
const slim        = require('gulp-slim');

const config      = require('../config.js'); // config.js の読み込み

gulp.task('slim', done => {
  gulp.src([config.src + '/*.slim'])
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    .pipe(slim({
      pretty: true,
      require: 'slim/include',
      options: 'include_dirs=["' + config.partial + '"]'
    }))
    .pipe(gulp.dest(config.build))
    .pipe(browserSync.stream());
    done();

  gulp.src([config.src + '/reform/*.slim'])
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    .pipe(slim({
      pretty: true,
      require: 'slim/include',
      options: 'include_dirs=["src/reform/_partial", "src/_partial"]'
    }))
    .pipe(gulp.dest(config.build))
    .pipe(browserSync.stream());
    done();
});
