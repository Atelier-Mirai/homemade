const gulp   = require('gulp');
const htmlv  = require('gulp-htmlhint');

const config = require('../config.js'); // config.js の読み込み

// html verify
gulp.task('htmlv', (done) => {
  gulp.src(config.build + '*.html')
  .pipe(htmlv())
  .pipe(htmlv.reporter());
  done();
});
