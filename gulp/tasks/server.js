const gulp        = require('gulp');
const browserSync = require('browser-sync').create();

const config      = require('../config.js');

gulp.task('server', done => {
  browserSync.init({
    server: {
      baseDir: config.build,
      index: "index.html"
    }
  })

  done();
});
