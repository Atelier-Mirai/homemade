const gulp     = require('gulp');
const imagemin = require('gulp-imagemin');

const config   = require('../config.js'); // config.js の読み込み

// imagemin
gulp.task('imagemin', (done) => {
  gulp.src(config.src_images + '**/*.{png,jpg,gif,svg}')
    .pipe(imagemin([
      imagemin.gifsicle({ interlaced: true }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.optipng({ optimizationLevel: 4 }),
      imagemin.svgo({
        plugins: [
          { removeViewBox: false }
        ]
      })
    ]))
    .pipe(gulp.dest(config.build_images));
  done();

  gulp.src('./src/reform/**/*.{png,jpg,gif,svg}')
    .pipe(imagemin([
      imagemin.gifsicle({ interlaced: true }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.optipng({ optimizationLevel: 4 }),
      imagemin.svgo({
        plugins: [
          { removeViewBox: false }
        ]
      })
    ]))
    .pipe(gulp.dest('./build/reform/'));
  done();
});
