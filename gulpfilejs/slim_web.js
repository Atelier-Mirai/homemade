/******************************************************************************
 * プラグインの読み込み
 ******************************************************************************/
const { src, dest, watch, series, parallel } = require("gulp")
const slim        = require('gulp-slim')
const browserSync = require('browser-sync').create()
const notify      = require('gulp-notify')
const plumber     = require('gulp-plumber')

const config      = require('./config') // ディレクトリ設定値 の読み込み

/******************************************************************************
 * slimをコンパイルし、htmlを生成する
 ******************************************************************************/
const slim2html = () =>
  src([`${config.src.html}web/*.slim`])
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    .pipe(slim({
      pretty: true,
      require: "slim/include",
      options: `include_dirs=['${config.src.partial}']`
    }))
    .pipe(dest(`${config.dist.html}web`))
    .pipe(browserSync.stream())

exports.slim_web = slim2html
