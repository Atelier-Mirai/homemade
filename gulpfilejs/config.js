// ディレクトリの定義
const dist        = 'dist/'
const src         = 'src/'

const images      = 'images/'
const javascripts = 'javascripts/'
const stylesheets = 'stylesheets/'
const partial     = '_partial/'

module.exports = {
  // 入力元
  src: { html:        `${src}`,
         images:      `${src}${images}`,
         javascripts: `${src}${javascripts}`,
         stylesheets: `${src}${stylesheets}`,
         partial:     `${src}${partial}` },
  // 出力先
  dist: { html:        `${dist}`,
          images:      `${dist}${images}`,
          javascripts: `${dist}${javascripts}`,
          stylesheets: `${dist}${stylesheets}` }
}
