/******************************************************************************
 * プラグインの読み込み
 ******************************************************************************/
const { src, dest, watch, series, parallel } = require("gulp")
const rename      = require("gulp-rename")
const concat      = require("gulp-concat")
const babel       = require("gulp-babel")
const uglify      = require("gulp-uglify")
const browserSync = require('browser-sync').create()
const notify      = require('gulp-notify')
const plumber     = require('gulp-plumber')

const config      = require('./config') // ディレクトリ設定値 の読み込み

/******************************************************************************
 * JavaScriptファイルを結合、圧縮して、all.min.jsを出力
 ******************************************************************************/
const jsMinify = () =>
  src(`${config.src.javascripts}*.js`)
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    // ES2015で書かれていたら、旧形式のjsにコンパイル
    .pipe(babel({
      presets: ['@babel/preset-env']
    }))
    // jsを圧縮
    .pipe(uglify())
    .pipe(concat('all.js'))
    .pipe(rename({suffix: '.min'}))
    // 出力ディレクトリ指定
    .pipe(dest(config.dist.javascripts))
    .pipe(browserSync.stream())
exports.jsmin = jsMinify
