/******************************************************************************
 * gulpプラグインの読み込み
 ******************************************************************************/
const { src, dest, watch, series, parallel } = require("gulp")

const { slim }     = require("./slim")
const { sass }     = require("./sass")
const { jsmin }    = require("./jsmin")
const { imagemin } = require("./imagemin")
const { md }       = require("./markdown")
const { pdf }      = require("./markdownpdf")

const config       = require('./config') // ディレクトリ設定値 の読み込み

/******************************************************************************
 * 一括生成
 ******************************************************************************/
exports.build = () =>
  // parallel(slim, sass, jsmin, imagemin, md, pdf)
  series(slim, sass, jsmin, imagemin, md, pdf)
