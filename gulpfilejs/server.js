/******************************************************************************
 * プラグインの読み込み
 ******************************************************************************/
const { src, dest, watch, series, parallel } = require("gulp")
const browserSync = require('browser-sync').create()
const config      = require('./config') // ディレクトリ設定値 の読み込み

/******************************************************************************
 * browserSync
 ******************************************************************************/
const server = () =>
  browserSync.init({
    notify: false,
    server: {
      baseDir: config.dist.html,
      // directory: true,
      serveStaticOptions: {
        extensions: ["html"]
      },
      index: "index.html",
      reloadOnRestart: false
    }
  })
exports.server = server
