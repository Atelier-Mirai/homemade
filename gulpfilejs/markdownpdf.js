/******************************************************************************
 * プラグインの読み込み
 ******************************************************************************/
const { src, dest, watch, series, parallel } = require("gulp")
const markdownpdf = require("gulp-markdown-pdf")
const config      = require('./config') // ディレクトリ設定値 の読み込み

/******************************************************************************
 * markdwon を htmlに
 ******************************************************************************/
const pdf = () =>
  src("*.md")
    .pipe(markdownpdf())
    .pipe(dest(config.dist.html))
exports.pdf = pdf
