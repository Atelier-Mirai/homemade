/******************************************************************************
 * 分割した各種タスクファイルを読み込み、パブリックタスクとして使う。
 ******************************************************************************/
const { slim }     = require("./slim")
exports.slim       = slim
const { slim_web }     = require("./slim_web")
exports.slim_web       = slim_web
const { sass }     = require("./sass")
exports.sass       = sass
const { jsmin }    = require("./jsmin")
exports.jsmin      = jsmin
const { imagemin } = require("./imagemin")
exports.imagemin   = imagemin
const { md }       = require("./markdown")
exports.md         = md
const { pdf }      = require("./markdownpdf")
exports.pdf        = pdf

// buid コマンド
// 配布用 html等を一括生成
const { build } = require("./build")
exports.build   = build

// browser-sync を用いて 生成したhtmlを同期
const { server } = require("./server")
exports.server   = server

// npx gulp で ファイルの変更監視
const { watch } = require("./watch")
exports.default = watch
