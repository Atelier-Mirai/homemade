/******************************************************************************
 * gulpプラグインの読み込み
 ******************************************************************************/
const { src, dest, watch, series, parallel } = require("gulp")

const { slim }  = require("./slim")
const { sass }  = require("./sass")
const { jsmin } = require("./jsmin")
const { md }    = require("./markdown")
const { pdf }   = require("./markdownpdf")

const config    = require('./config') // ディレクトリ設定値 の読み込み

/******************************************************************************
 * ファイル変更監視
 ******************************************************************************/
exports.watch = () =>
  watch([`${config.src.html}*.slim`, `${config.src.partial}*.slim`], slim)
  watch('src/stylesheets/*.scss', sass)
  watch('src/javascripts/*.js', jsmin)
  watch('*.md', parallel(md, pdf))
