/******************************************************************************
 * プラグインの読み込み
 ******************************************************************************/
const { src, dest, watch, series, parallel } = require("gulp")
const imagemin = require("gulp-imagemin")
const config   = require('./config') // ディレクトリ設定値 の読み込み

/******************************************************************************
 * 画像ファイルの最適化
 ******************************************************************************/
const imageMinify = () =>
  src(`${config.src.images}*.*`)
    .pipe(imagemin([
      imagemin.gifsicle({ interlaced: true }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.optipng({ optimizationLevel: 4 }),
      imagemin.svgo({
        plugins: [
          { removeViewBox: false }
        ]
      })
    ]))
    .pipe(dest(config.dist.images))
exports.imagemin = imageMinify
