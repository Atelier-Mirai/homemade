/******************************************************************************
 * プラグインの読み込み
 ******************************************************************************/
const { src, dest, watch, series, parallel } = require("gulp")
const rename       = require("gulp-rename")
const concat       = require("gulp-concat")
const sass         = require("gulp-sass")
const postcss      = require('gulp-postcss')
const autoprefixer = require('autoprefixer')
const cssmin       = require("gulp-cssmin")
const browserSync  = require('browser-sync').create()
const notify      = require('gulp-notify')
const plumber     = require('gulp-plumber')

const config      = require('./config') // ディレクトリ設定値 の読み込み

/******************************************************************************
 * srcディレクトリのscssをコンパイル、結合、圧縮して、
 * distディレクトリにstyle.min.cssを生成する
 ******************************************************************************/
const compileSass = () =>
  // ソースファイルの指定
  src(`${config.src.stylesheets}*.scss`)
    .pipe(plumber({
      errorHandler: notify.onError('Error: <%= error.message %>')
    }))
    .pipe(
      sass({
        // コンパイル結果の出力形式指定
        outputStyle: "expanded"
        // outputStyle: "compressed"
      })
      // コンパイルエラーがあれば表示
      .on("error", sass.logError)
    )
    // ベンダープリフィックスの付与
    .pipe(postcss([
      autoprefixer({
        grid: true,
        browers: [
        '> 1% in JP'
        ]
      })
     ]))
    // cssを圧縮
    .pipe(cssmin())
    .pipe(concat('style.css'))
    .pipe(rename({suffix: '.min'}))
    // 出力ディレクトリ指定
    .pipe(dest(config.dist.stylesheets))
    .pipe(browserSync.stream())
// npx gulp sass コマンド実行で、sass(SCSS)がコンパイルされるようにする
exports.sass = compileSass
