# include template
require './ikadzuchi.rb'

# construction_name = "works"
construction_name = ARGV[0]

if construction_name.nil?
  # 全ての工事の施行事例を生成
  Ikadzuchi::generate_all
else
  # 個別の工事の施行事例を生成
  Ikadzuchi::generate(construction_name)
end
